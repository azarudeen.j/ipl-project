const csv = require('csvtojson');
const fs = require('fs');

const csvFilePath1 = './src/data/matches.csv';
const csvFilePath2 = './src/data/deliveries.csv';

const jsonFilePath1 = './src/data/matches.json';
const jsonFilePath2 = './src/data/deliveries.json'


function csvToJson(filePath,fileName){
    csv()
    .fromFile(filePath)
    .then((json)=>{  
    fs.writeFile(fileName, JSON.stringify(json), (err) => { 
        if (err) console.log(err)
    }) 
})
    
}

csvToJson(csvFilePath1,jsonFilePath1);
csvToJson(csvFilePath2,jsonFilePath2);






